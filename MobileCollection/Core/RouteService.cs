﻿using System;
using System.Collections.Generic;
namespace MobileCollection
{
	public class RouteService
	{
		private FakeRoute data;

		public RouteService()
		{
			data = new FakeRoute();
		}

		public IEnumerable<RouteModel> Get() 
		{
			return data.GetRoutes();
		}

	}
}
