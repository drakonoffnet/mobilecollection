﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace MobileCollection
{
	public class EmployeeService
	{
		private FakeEmployee fakeData;

		public EmployeeService()
		{
			fakeData = new FakeEmployee();
		}

		public List<EmployeeModel> GetAll(Guid tripId) 
		{
			return fakeData.Get().ToList();
		}
	}
}
