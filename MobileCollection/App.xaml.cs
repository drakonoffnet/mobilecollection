﻿using Xamarin.Forms;
using MobileCollection.Views;

namespace MobileCollection
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			var page = new NavigationPage(new MobileCollectionPage());

			page.BarBackgroundColor = GonfigStyle.ColorsBackgroundColor.Menu;
			page.BarTextColor = Color.White;
			MainPage = page;
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
