﻿using System;
namespace MobileCollection
{
	public class RouteModel
	{
		public Guid Id { get; set; }
		
		public string Name { get; set; }

		public bool IsDone { get; set; }

		public int Point { get; set; }
	}
}
