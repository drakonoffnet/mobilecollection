﻿using System;
using System.Collections.Generic;
namespace MobileCollection
{
	public class FakeEmployee
	{
		public IEnumerable<EmployeeModel> Get() 
		{
			List<EmployeeModel> list = new List<EmployeeModel>();

			list.Add(new EmployeeModel() { Name = "Сидоров И.И." });
			list.Add(new EmployeeModel() { Name = "Петров Ф.К." });
			list.Add(new EmployeeModel() { Name = "Рабинович Г.К." });
			list.Add(new EmployeeModel() { Name = "Иванов Д.А." });

			return list;
		}
	}
}
