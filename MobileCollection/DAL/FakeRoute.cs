﻿using System;
using System.Collections.Generic;
namespace MobileCollection
{
	public class FakeRoute
	{
		public IEnumerable<RouteModel> GetRoutes() 
		{
			List<RouteModel> list = new List<RouteModel>();

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "1.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "1.2" });

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = true, Name = "2.1", Point = 12 });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "2.2", Point = 2 });
										   
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "3.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "3.2" });
										   
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "4.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "4.2" });
										   
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "5.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "5.2" });
										    
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "6.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "6.2" });

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = true, Name = "7.1", Point = 25 });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "7.2" });

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "8.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "8.2" });

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "9.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "9.2" });

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "10.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "10.2" });

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "11.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "11.2" });

			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "12.1" });
			list.Add(new RouteModel { Id = Guid.NewGuid(), IsDone = false, Name = "12.2" });

			return list;
		}
	}
}
