﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace MobileCollection
{
	public class RoutesPage : ContentPage
	{
		private Grid grid;
		private RouteService routeService;
		private IEnumerable<RouteModel> routs;
		public RoutesPage()
		{
			this.BackgroundColor = GonfigStyle.ColorsBackgroundColor.App;
			this.Title = "Маршруты";

			routeService = new RouteService();
			routs = routeService.Get();

			grid = new Grid();

			LoadRouts();

			var view = new StackLayout();

			var scrolView = new ScrollView()
			{
				Content = grid,
				Padding = new Thickness(20, 20, 20, 120)

			};

			view.Children.Add(scrolView);
			view.Children.Add(BottomMenu.Get());

			Content = view;

		}

		public void LoadRouts() 
		{
			double heightItem = 30;

			int row = 0;
			int index = 0;
			foreach (var route in routs) 
			{
				int column = 0;

				if (index % 2 != 0) 
				{
					column = 1;
				}

				var item = GetItemRout(heightItem, route);

				if (route.IsDone) 
				{
					item.BackgroundColor = GonfigStyle.ColorsBackgroundColor.RouteSelect;
				}

				grid.Children.Add(item, column, row);

				if (index % 2 != 0) 
				{
					row++;
				}

				index++;
			}
		}

		View GetItemRout(double height, RouteModel route) 
		{
			string nameRoute = route.Name;
			if (route.Point > 0)
			{
				nameRoute = string.Format("{0} ({1})", route.Name, route.Point);

			}

			var labael = FormControl.GetLabel(nameRoute, height, TextAlignment.Center);

			if (!route.IsDone && route.Point > 0)
			{
				var action = new TapGestureRecognizer();
				action.Tapped += (sender, e) => 
				{
					var nextPage = new EmployeePage(route);

					Navigation.PushAsync(nextPage, false);
				};
				labael.GestureRecognizers.Add(action);
			}

			return FormControl.GetFrame(labael, height);
		}

	}
}

