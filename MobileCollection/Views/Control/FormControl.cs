﻿using System;

using Xamarin.Forms;

namespace MobileCollection
{
	public class FormControl
	{
		/// <summary>
		/// Gets the label.
		/// </summary>
		/// <returns>The label.</returns>
		/// <param name="text">label text.</param>
		/// <param name="fontSize">Font size.</param>
		/// <param name="height">Height.</param>
		/// <param name="textAling">Text aling.</param>
		/// <param name="color">Color.</param>
		public static Label GetLabel(string text, double fontSize, double height, TextAlignment textAling, Color color)
		{
			return new Label()
			{
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = textAling,
				HeightRequest = height,
				Text = text,
				FontSize = fontSize,
				TextColor = Color.White
			};
		}

		/// <summary>
		/// Gets the label.
		/// default: font size 20px, Height 30px
		/// </summary>
		/// <returns>The label.</returns>
		/// <param name="text">label text</param>
		public static Label GetLabel(string text) 
		{
			return GetLabel(text, 20, 30, TextAlignment.Start, Color.White);
		}

		/// <summary>
		/// Gets the label.
		/// default: font size 20px, Height 30px
		/// </summary>
		/// <returns>The label.</returns>
		/// <param name="text">label text.</param>
		/// <param name="textAling">Text aling.</param>
		public static Label GetLabel(string text, TextAlignment textAling)
		{
			return GetLabel(text, 20, 30, textAling, Color.White);
		}

		/// <summary>
		/// Gets the label.
		/// default: font size 20px
		/// </summary>
		/// <returns>The label.</returns>
		/// <param name="text">label text.</param>
		/// <param name="height">Height.</param>
		/// <param name="textAling">Text aling.</param>
		public static Label GetLabel(string text, double height, TextAlignment textAling)
		{
			return GetLabel(text, 20, height, textAling, Color.White);
		}

		public static Frame GetFrame(View element, double height)
		{
			return new Frame
			{
				Content = element,
				OutlineColor = Color.White,
				BackgroundColor = GonfigStyle.ColorsBackgroundColor.App,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				HeightRequest = height,
				HasShadow = true
			};
		}

		public static Button GetButton(string text) 
		{
			return new Button() 
			{
				BackgroundColor = GonfigStyle.GlobalColor.Gren,
				Text = text,
				TextColor = Color.White,
				Margin = new Thickness(0, 20, 0, 0)
			};
		}

		public static Button GetButton(string text, Button button)
		{
			button.BackgroundColor = GonfigStyle.GlobalColor.Gren;
			button.Text = text;
			button.TextColor = Color.White;
			button.Margin = new Thickness(0, 20, 0, 0);
			return button;
		}
	}
}

