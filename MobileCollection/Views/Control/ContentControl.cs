﻿using System;
using Xamarin.Forms;

namespace MobileCollection
{
	public class ContentControl
	{
		/// <summary>
		/// Gets the square image.
		/// </summary>
		/// <returns>The square image.</returns>
		/// <param name="nameFile">Name file.</param>
		/// <param name="size">Size.</param>
		public static Image GetSquareImage(string nameFile, double size)
		{
			return new Image()
			{
				Source = Device.OnPlatform(
							iOS: ImageSource.FromFile(nameFile),
							Android: ImageSource.FromFile(nameFile),
							WinPhone: ImageSource.FromFile(nameFile)),
				HeightRequest = size,
				WidthRequest = size
			};
		}

		/// <summary>
		/// Gets the image.
		/// </summary>
		/// <returns>The image.</returns>
		/// <param name="nameFile">Name file.</param>
		/// <param name="sizeHeight">Size height.</param>
		/// <param name="sizeWidth">Size width.</param>
		public static Image GetImage(string nameFile, double sizeHeight, double sizeWidth)
		{
			return new Image()
			{
				Source = Device.OnPlatform(
							iOS: ImageSource.FromFile(nameFile),
							Android: ImageSource.FromFile(nameFile),
							WinPhone: ImageSource.FromFile(nameFile)),
				HeightRequest = sizeHeight,
				WidthRequest = sizeWidth
			};
		}

		public static BoxView GetItemBox(double height)
		{
			return new BoxView()
			{
				BackgroundColor = Color.White,
				HeightRequest = height,
			};
		}
	}
}
