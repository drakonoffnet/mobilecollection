﻿using System;
using Xamarin.Forms;
namespace MobileCollection
{
	public class BottomMenu
	{
		public static Grid Get() 
		{
			var btnSync = GetImage("btn_sync.png");
 
			var btnRoute = GetImage("btn_route.png");

			var btnCar = GetImage("btn_car.png");

			var btnDone = GetImage("btn_done.png");

			var grid = new Grid();
			grid.HorizontalOptions = LayoutOptions.Fill;
			grid.BackgroundColor = GonfigStyle.ColorsBackgroundColor.Menu;
			grid.HeightRequest = 120;

			grid.Children.Add(btnSync, 0, 0);
			grid.Children.Add(btnRoute, 1, 0);
			grid.Children.Add(btnCar, 2, 0);
			grid.Children.Add(btnDone, 3, 0);
			 
			return grid;
		}

		private static Image GetImage(string nameFile) 
		{
			var image = ContentControl.GetSquareImage(nameFile, 60);
			image.HorizontalOptions = LayoutOptions.Center;
			image.VerticalOptions = LayoutOptions.Center;
			image.Margin = new Thickness(0, 10, 0, 10);
			return image;
		}
	}
}
