﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MobileCollection
{
	public partial class EmployeePage : ContentPage
	{
		public RouteModel DetailTrip { get; set; }

		private EmployeeService employeeService;
		private List<EmployeeModel> employee;
		public EmployeePage(RouteModel detailTrip)
		{
			InitializeComponent();


			DetailTrip = detailTrip;

			this.Title = string.Format("Маршрут №{0}", DetailTrip.Name);
			this.BackgroundColor = GonfigStyle.ColorsBackgroundColor.App;

			employeeService = new EmployeeService();

			employee = employeeService.GetAll(DetailTrip.Id);

			var selectEmplOne = new TapGestureRecognizer();
			selectEmplOne.Tapped += SelectEmplOne_Tapped;

			emplOne.GestureRecognizers.Add(selectEmplOne);
			emplOne.Text = employee[0].Name;

			emplTwo.Text = employee[1].Name;

			emplThree.Text = employee[2].Name;

			FormControl.GetButton("Выехать на маршрут", this.StartTrip);
		}

		void SelectEmplOne_Tapped(object sender, EventArgs e)
		{
			if (employee != null)
			{
				picker = GetPicer("Color", employee);
				picker.IsVisible = true;
			}
		}


		private Picker GetPicer(string title, IList<EmployeeModel> entity)
		{
			Picker picker = new Picker
			{
				Title = title,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			foreach (EmployeeModel item in entity)
			{
				picker.Items.Add(item.Name);
			}

			return picker;
		}
	}
}
