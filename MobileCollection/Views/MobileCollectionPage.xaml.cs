﻿using Xamarin.Forms;

namespace MobileCollection.Views
{
	public partial class MobileCollectionPage : ContentPage
	{
		public MobileCollectionPage()
		{
			InitializeComponent();
			this.BackgroundColor = GonfigStyle.ColorsBackgroundColor.App;

			this.Title = "Авторизация";
			NavigationPage.SetHasNavigationBar(this, false);


			this.logoApp.Margin = new Thickness(0, 20, 0, 0);
			this.logoApp.HeightRequest = 50;

			var labelName = this.labelName;
			labelName.Text = "Мобильная инкасация";
			labelName.Margin = new Thickness(0, 10, 0, 0);
			labelName.HorizontalOptions = LayoutOptions.Center;
			labelName.TextColor = Color.White;

			var btn = FormControl.GetButton("Войти", this.BtnSingIn);
			btn.Clicked += BtnSingIn_Clicked;
			 

		}

		async void BtnSingIn_Clicked(object sender, System.EventArgs e)
		{
			if (Verification()) 
			{
				var nextPage = new RoutesPage();
				await Navigation.PushAsync(nextPage, false);
				 
			}
		}

		bool Verification() 
		{
			if (string.IsNullOrEmpty(UserLogin.Text)) 
			{
				 DisplayAlert("Авторизация", "Укажите логин", "OK");
				return false;
			}

			if (string.IsNullOrEmpty(UserLogin.Text))
			{
				DisplayAlert("Авторизация", "Укажите пароль", "OK");
				return false;
			}
			return true;
		}
		 
	}
}
