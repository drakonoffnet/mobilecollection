﻿using System;
using Xamarin.Forms;

namespace MobileCollection
{
	public class GonfigStyle
	{
		public class ColorsBackgroundColor 
		{
			public static Color App = Color.FromRgb(44, 63, 133);

			public static Color Menu = Color.FromRgb(0, 33, 87);

			public static Color RouteSelect = Color.FromRgb(0, 193, 243);

		}

		public class GlobalColor 
		{
			public static Color Gren = Color.FromRgb(25, 123, 48);
		}
	}
}
