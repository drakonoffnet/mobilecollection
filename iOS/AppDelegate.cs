﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace MobileCollection.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			//UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
			//{
			//	TextColor = UIColor.White,

			//});

			app.SetStatusBarStyle(UIStatusBarStyle.LightContent, true);
			 
			global::Xamarin.Forms.Forms.Init();

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}
